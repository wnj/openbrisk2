/*
 * Copyright 2013 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/* =========================================================================== *
 * This file would be modified if an alternative to malloc, free functions from
 * the standard C library is provided (e.g. jemalloc).
 *
 * NOTE:
 * On realtime operating systems, a non-thread-safe memory allocator and a 
 * private memory pool would be applied to improve the realtime performance.
 * ========================================================================== *
 */

#include <BRISK/include/BriskMemory.h>

/* using tlsf memory allocator, thus we need a global tlsf memory pool. */
tlsf_pool BriskMemoryGlobal;

void *_brisk_mem_pool;

bool briskMemoryInitialize(const b_size_t size, enum BriskMemoryUnits unit)
{
    const b_size_t bytes = size * unit;
    _brisk_mem_pool = malloc(bytes);
    return (BriskMemoryGlobal = tlsf_create(_brisk_mem_pool, bytes));
}

void briskMemoryRelease()
{
    free(_brisk_mem_pool);
    tlsf_destroy(BriskMemoryGlobal);
}
