/*
 * Copyright 2013 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <BRISK/include/BriskPrototypes.h>

struct BriskLayer briskLayerCreate(BriskImage *image,
                                   b_float_t scale, b_float_t offset)
{
    struct BriskLayer briskLayer;
    
    briskLayer.image = image;
    
    briskLayer.scale = scale;
    briskLayer.offset = offset;
    
    briskLayer.scores = (BriskImage *)BRISK_MALLOC(sizeof(BriskImage));
    BRISK_MALLOC_FAIL_CHECK(briskLayer.scores);
    
    briskLayer.scores->width = image->width;
    briskLayer.scores->height = image->height;
    
    b_size_t length = image->width * image->height * sizeof(b_uint8_t);
    briskLayer.scores->pixel = (b_uint8_t *)BRISK_MALLOC(length);
    BRISK_MALLOC_FAIL_CHECK(briskLayer.scores->pixel);
    BRISK_MEMSET(briskLayer.scores->pixel, 0, length);
    
    return briskLayer;
}

struct BriskLayer briskLayerDerive(const struct BriskLayer *layer, b_int_t mode)
{
    struct BriskLayer briskLayer;
    
    if(mode == LAYER_HALF_SAMPLE)
    {
        briskLayer.image =
            briskImageAllocWithSize(layer->image->width / 2,
                                    layer->image->height / 2,
                                    BRISK_IMAGE_FORMAT_GRAY);
        
        briskLayer.scale = layer->scale * 2.0f;
        
        briskLayerHalfSample(layer->image, briskLayer.image);
    }
    else
    {
        briskLayer.image =
            briskImageAllocWithSize(layer->image->width / 3 * 2,
                                    layer->image->height / 3 * 2,
                                    BRISK_IMAGE_FORMAT_GRAY);
        
        briskLayer.scale = layer->scale * 1.5f;
        
        briskLayerTwoThirdSample(layer->image, briskLayer.image);
    }
    
    briskLayer.offset = 0.5f * briskLayer.scale - 0.5f;
    
    briskLayer.scores = (BriskImage *)BRISK_MALLOC(sizeof(BriskImage));
    BRISK_MALLOC_FAIL_CHECK(briskLayer.scores);
    
    briskLayer.scores->width = briskLayer.image->width;
    briskLayer.scores->height = briskLayer.image->height;
    briskLayer.scores->format = BRISK_IMAGE_FORMAT_GRAY;
    
    b_size_t length = briskLayer.image->width *
    briskLayer.image->height * sizeof(b_uint8_t);
    
    briskLayer.scores->pixel = (b_uint8_t *)BRISK_MALLOC(length);
    BRISK_MALLOC_FAIL_CHECK(briskLayer.scores->pixel);
    BRISK_MEMSET(briskLayer.scores->pixel, 0, length);
    
    return briskLayer;
}

static b_uint8_t value(const BriskImage *mat,
                       b_float_t xf, b_float_t yf, b_float_t scale)
{
    assert(mat);
    
    const b_int_t x = floorf(xf);
    const b_int_t y = floorf(yf);
    const BriskImage *image = mat;
    const b_int_t imagecols = image->width;
    
    const b_float_t sigmaHalf = scale / 2.0f;
    const b_float_t area = (b_float_t)(4.0 * sigmaHalf * sigmaHalf);

    b_int_t result;
    
    if(sigmaHalf < 0.5f)
    {
        const b_int_t rx = (xf - x) * 1024;
        const b_int_t ry = (yf - y) * 1024;
        const b_int_t rx1 = (1024 - rx);
        const b_int_t ry1 = (1024 - ry);
        
        b_uint8_t *ptr = image->pixel + x + y * imagecols;
        
        result  = (rx1 * ry1 * (b_int_t)*ptr);  ptr++;
        result += (rx  * ry1 * (b_int_t)*ptr);  ptr += imagecols;
        result += (rx  * ry  * (b_int_t)*ptr);  ptr--;
        result += (rx1 * ry  * (b_int_t)*ptr);
        
        return 0xff & ((result + 512) / 1024 / 1024);
    }
    
    const b_int_t scaling = 4194304.0f / area;
    const b_int_t scaling2 = (b_float_t)(scaling) * area / 1024.0f;
    
    const b_float_t x_1 = xf - sigmaHalf;
    const b_float_t x1  = xf + sigmaHalf;
    const b_float_t y_1 = yf - sigmaHalf;
    const b_float_t y1  = yf + sigmaHalf;
    
    const b_int_t x_left   = (b_int_t)(x_1 + 0.5f);
    const b_int_t y_top    = (b_int_t)(y_1 + 0.5f);
    const b_int_t x_right  = (b_int_t)(x1  + 0.5f);
    const b_int_t y_bottom = (b_int_t)(y1  + 0.5f);
    
    const b_float_t r_x_1 = (b_float_t)(x_left) - x_1  + 0.5f;
    const b_float_t r_y_1 = (b_float_t)(y_top) - y_1   + 0.5f;
    const b_float_t r_x1  = x1 - (b_float_t)(x_right)  + 0.5f;
    const b_float_t r_y1  = y1 - (b_float_t)(y_bottom) + 0.5f;
    
    const b_int_t dx = x_right - x_left - 1;
    const b_int_t dy = y_bottom - y_top - 1;
    
    const b_int_t A = (r_x_1 * r_y_1) * scaling;
    const b_int_t B = (r_x1 * r_y_1) * scaling;
    const b_int_t C = (r_x1 * r_y1) * scaling;
    const b_int_t D = (r_x_1 * r_y1) * scaling;
    
    const b_int_t r_x_1_i = r_x_1 * scaling;
    const b_int_t r_y_1_i = r_y_1 * scaling;
    const b_int_t r_x1_i  = r_x1 * scaling;
    const b_int_t r_y1_i  = r_y1 * scaling;
    
    b_uint8_t *ptr = image->pixel + x_left + imagecols * y_top;

    result = A * (b_int_t)(*ptr);
    ptr++;
    const b_uint8_t *end1 = ptr + dx;
    
    for(; ptr<end1; ptr++)
        result += r_y_1_i * (b_int_t)(*ptr);
    
    result += B * (b_int_t)(*ptr);

    ptr += imagecols - dx - 1;
    b_uint8_t *end_j = ptr + dy * imagecols;
    
    for(; ptr < end_j; ptr += imagecols - dx - 1)
    {
        result += r_x_1_i * (b_int_t)(*ptr);
        ptr++;
        const b_uint8_t *end2 = ptr + dx;
        
        for(; ptr < end2; ptr++)
            result += (b_int_t)(*ptr) * scaling;
        
        result += r_x1_i * (b_int_t)(*ptr);
    }

    result += D * (b_int_t)(*ptr);
    ptr++;
    const b_uint8_t *end3 = ptr + dx;
    
    for(; ptr<end3; ptr++)
        result += r_y1_i * (b_int_t)(*ptr);
    
    result += C * (b_int_t)(*ptr);
    
    return 0xff & ((result+scaling2 / 2) / scaling2 / 1024);
}

BriskPoint *getAgastPoints(struct BriskLayer *layer,
                           b_uint8_t threshold, b_int_t *length)
{
    BriskPoint *featurePoint =
        (BriskPoint *)oast9_16(layer->image->pixel, layer->image->width,
                               layer->image->height, threshold, length);
    
    const b_int_t num = *length;
    const b_int_t imcols = layer->image->width;
    
    b_size_t i;
    
    for(i = 0; i < num; i++)
    {
        const b_int_t offset = featurePoint[i].x + featurePoint[i].y * imcols;
        
        *(layer->scores->pixel + offset) =
            oast9_16_cornerScore(layer->image->pixel + offset, threshold);
    }
    
    return featurePoint;
}

b_uint8_t getAgastScore(struct BriskLayer *layer, b_int_t x, b_int_t y,
                        b_uint8_t threshold)
{
    if(x < 3 || y < 3)
        return 0;
    if(x >= layer->image->width - 3 || y >= layer->image->height-3)
        return 0;
    
    b_uint8_t score = *(layer->scores->pixel + x + y * layer->scores->width);
    if(score > 2)
        return score;
    
    oast9_16_cornerScore(layer->image->pixel + x + y * layer->image->width,
                         threshold - 1);

    if (score<threshold) score = 0;
    return score;
}

b_uint8_t getAgastScore_5_8(struct BriskLayer *layer, b_int_t x, b_int_t y,
                            b_uint8_t threshold)
{
    if(x < 2 || y < 2)
        return 0;
    
    if(x >= layer->image->width-2 || y >= layer->image->height-2)
        return 0;
    
    b_uint8_t score = agast5_8_cornerScore(layer->image->pixel + x + y
                                           * layer->image->width,
                                           threshold - 1);
    
    if(score < threshold)
        score = 0;
    return score;
}

b_uint8_t getAgastScore2(struct BriskLayer *layer, b_float_t xf, b_float_t yf,
                         b_uint8_t threshold, b_float_t scale)
{
    b_int_t x, y;
    
    if(scale <= 1.0f)
    {
        const b_int_t x = xf;
        const int y = yf;

        const b_float_t rx1 = xf - (b_float_t)x;
        const b_float_t ry1 = yf - (b_float_t)y;
        
        const b_float_t rx = 1.0f - rx1;
        const b_float_t ry = 1.0f - ry1;
        
        return   rx  * ry  * getAgastScore(layer, x,     y,     threshold)
               + rx1 * ry  * getAgastScore(layer, x + 1, y,     threshold)
               + rx  * ry1 * getAgastScore(layer, x,     y + 1, threshold)
               + rx1 * ry1 * getAgastScore(layer, x + 1, y + 1, threshold);
    }
    else
    {
        const b_float_t halfScale = scale / 2.0f;

        for(x = (b_int_t)(xf - halfScale);
            x <= (b_int_t)(xf + halfScale + 1.0f); x++)
        {
            for(y = (b_int_t)(yf - halfScale);
                y <= (b_int_t)(yf + halfScale + 1.0f); y++)
            {
                getAgastScore(layer, x, y, threshold);
            }
        }

        return value(layer->scores, xf, yf, scale);
    }
}
