/*
 * Copyright 2013 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 * =========================================================================== *
 * This file would be modified if an alternative to malloc, free functions from
 * the standard C library is provided.
 *
 * NOTE:
 * On realtime operating systems, a non-thread-safe memory allocator and 
 * a private memory pool would be applied to improve the realtime performance.
 * ========================================================================== *
 */

#include <stdio.h>
#include <stdlib.h>

/* tlsf memory allocator */
#include <BRISK/3rd-party/tlsf/tlsf.h>
#include <BRISK/include/BriskTypes.h>

#ifndef __BRISK_MEMORY_H__
#define __BRISK_MEMORY_H__

extern tlsf_pool BriskMemoryGlobal;

#ifdef BRISK_MALLOC
#   undef BRISK_MALLOC
#endif /* BRISK_MALLOC */
#define BRISK_MALLOC(_size)        tlsf_malloc(BriskMemoryGlobal, _size)

#ifdef BRISK_REALLOC
#   undef BRISK_REALLOC
#endif /* BRISK_REALLOC*/
#define BRISK_REALLOC(_ptr, _size) tlsf_realloc(BriskMemoryGlobal, _ptr, _size)

#ifdef BRISK_MFREE
#   undef BRISK_MFREE
#endif /* BRISK_MFREE*/
#define BRISK_MFREE(_ptr)          tlsf_free(BriskMemoryGlobal, _ptr)


#ifdef BRISK_MEMCPY
#   undef BRISK_MEMCPY
#endif /* BRISK_MEMCPY */
#define BRISK_MEMCPY memcpy

#ifdef BRISK_MEMSET
#   undef BRISK_MEMSET
#endif /* BRISK_MEMSET */
#define BRISK_MEMSET memset

#define BRISK_MALLOC_FAIL_CHECK(__ptr_in)                                 \
do {                                                                      \
    if(!(__ptr_in))                                                       \
    {                                                                     \
        fprintf(stderr, "BRISK: fatal: memory allocation faild in %s, "   \
                        "file: %s, line: %d. exit.\n",                    \
                __FUNCTION__, __FILE__, __LINE__);                        \
        exit(1);                                                          \
    }                                                                     \
} while(0)

enum BriskMemoryUnits
{
    KiloBytes = 1024,
    MegaBytes = 1048576
};

/* initialize the tlsf memory allocator */
bool briskMemoryInitialize(const b_size_t size, enum BriskMemoryUnits unit);
/* memory pool & allocator cleanup */
void briskMemoryRelease();

#endif /* __BRISK_MEMORY_H__ */
